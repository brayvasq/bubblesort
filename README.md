# Bubble sort animation

## Creación del proyecto

| Herramienta | Version | Descripción                                                  |
| ----------- | ------- | ------------------------------------------------------------ |
| Java        | 11.0.6  | Lenguaje de programación                                     |
| Gradle      | 6.1.1   | Herramienta para la gestión y creación de proyectos JAVA, automatización de tareas y con configuración basada en Groovy |

```bash
# Versión de gradle
gradle -v

# Crear directorio
mkdir bubblesort
cd bublesort

# Inicializar proyecto gradle
gradle init

Select type of project to generate:
  1: basic
  2: application
  3: library
  4: Gradle plugin
Enter selection (default: basic) [1..4] 2

Select implementation language:
  1: C++
  2: Groovy
  3: Java
  4: Kotlin
  5: Swift
Enter selection (default: Java) [1..5] 4

Select build script DSL:
  1: Groovy
  2: Kotlin
Enter selection (default: Kotlin) [1..2] 1

Project name (default: bubblesort):    
Source package (default: bubblesort): com.brayvasq.bubblesort

# See tasks
gradle tasks
./gradlew tasks

# Compile
./gradlew build

# Run
./gradlew run

# Clean build  folder
./gradlew clean

# Run tests
./gradlew test
```

## Creando Frame

Crear el paquete `views`, quedando la siguiente estructura `com.brayvasq.bubblesort.views`. 

Crear el archivo `Frame.kt` en el paquete `views`, quedando la estructura así:

```bash
src/
├── main
│   ├── kotlin
│   │   └── com
│   │       └── brayvasq
│   │           └── bubblesort
│   │               ├── App.kt
│   │               └── views
│   │                   └── Frame.kt
│   └── resources
└── test
```

Archivo `Frame.kt`

```kotlin
package com.brayvasq.bubblesort.views

import java.awt.Dimension
import javax.swing.JFrame

/**
 * Frame that contains the elements of the App
 */
class Frame: JFrame() {
    /**
     * Frame contructor
     */
    init { createUI() }

    /**
     * Setup the JFrame properties
     */
    private fun createUI() {
        // Set windows title
        title = "BubbleSort"
        // Set window size
        size = Dimension(800,300)
        // Allow to close the window
        defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        // To center the window
        setLocationRelativeTo(null)
        // To not allow resize the window
        isResizable = false
        // To show the window
        isVisible = true
    }
}
```

Archivo `App.kt`

```kotlin
/*
 * This Kotlin source file was generated by the Gradle 'init' task.
 */
package com.brayvasq.bubblesort

import com.brayvasq.bubblesort.views.Frame

class App {
    val greeting: String
        get() {
            return "Hello world."
        }
}

fun main(args: Array<String>) {
    println(App().greeting)
    Frame()
}
```

Ejecutando el proyecto

```bash
./gradlew clean build run
```

## Creando Panel

Crear el archivo `Panel.kt` en el paquete `views`, quedando la estructura así:

```bash
src/
├── main
│   ├── kotlin
│   │   └── com
│   │       └── brayvasq
│   │           └── bubblesort
│   │               ├── App.kt
│   │               └── views
│   │                   ├── Frame.kt
│   │                   └── Panel.kt
│   └── resources
└── test
```

Archivo `Panel.kt`

```kotlin
package com.brayvasq.bubblesort.views

import java.awt.Color
import java.awt.Dimension
import javax.swing.JPanel

/**
 * Panel that contains the elements drawed
 */
class Panel: JPanel(){

    /**
     * Styles and colors vars
     */
    var principal: Color
    var secondary: Color

    /**
     * Panel contructor
     */
    init {
        principal = Color(52,73,94)
        secondary = Color(236,240,241)

        createPanel()
    }

    /**
     * Setup the JPanel properties
     */
    private fun createPanel(){
        layout = null
        setSize(Dimension(600,300))
        background = principal
    }
}
```

Archivo `Frame.kt`

```kotlin
package com.brayvasq.bubblesort.views

import java.awt.BorderLayout
import java.awt.Dimension
import javax.swing.JFrame

/**
 * Frame that contains the elements of the App
 */
class Frame: JFrame() {
    var panel: Panel

    /**
     * Frame contructor
     */
    init {
        panel = Panel()
        createUI()
    }

    /**
     * Setup the JFrame properties
     */
    private fun createUI() {
		....
        // Setup panel
        add(panel, BorderLayout.CENTER)
        // To show the window
        isVisible = true
    }
}
```

## Creando Nodo (Lista)

Crear el paquete `models`, quedando la siguiente estructura `com.brayvasq.bubblesort.models`. 

Crear el archivo `Node.kt` en el paquete `models`, quedando la estructura así:

```kotlin
src/
├── main
│   ├── kotlin
│   │   └── com
│   │       └── brayvasq
│   │           └── bubblesort
│   │               ├── App.kt
│   │               ├── models
│   │               │   └── Node.kt
│   │               └── views
│   │                   ├── Frame.kt
│   │                   └── Panel.kt
│   └── resources
└── test
```

Archivo `Node.kt`

```kotlin
package com.brayvasq.bubblesort.models

import java.awt.Color
import java.awt.Graphics2D

/**
 * Class that represent a item from an array
 */
class Node(data: Int, posX: Int, posY: Int, tam: Int, primary: Color) {
    var data: Int = 0
    var posX: Int = 0
    var posY: Int = 0
    var tam:  Int = 0
    var primary: Color = Color(0,0,0)

    /**
     * Node class constructor
     */
    init {
        this.data = data
        this.posX = posX
        this.posY = posY
        this.tam  = tam
        this.primary = primary
    }

    /**
     * Function to draw a node
     */
    fun drawNode(g: Graphics2D){
        g.color = this.primary
        g.drawRect(this.posX - 2, this.posY, this.tam + 4, 24)

        g.color = this.primary
        g.drawString("${this.data}", this.posX + 10, this.posY + 15)
    }
}
```

## Dibujando en el Panel

Archivo `Panel.kt`

```kotlin
package com.brayvasq.bubblesort.views

import com.brayvasq.bubblesort.models.Node
import java.awt.*
import java.awt.RenderingHints
import javax.swing.JPanel


/**
 * Panel that contains the elements drawed
 */
class Panel: JPanel(), Runnable{

    /**
     * Styles and colors vars
     */
    var principal: Color
    var secondary: Color
    var square: Color
    var arrows: Color

    var wait: Boolean
    var thread: Thread
    var posI: Int
    var posJ: Int
    var posK: Int

    var vector: Array<Node>
    /**
     * Panel contructor
     */
    init {
        principal = Color(45,45,45)
        secondary = Color(236,240,241)
        square = Color(52, 152, 219)
        arrows = Color(231, 76, 60)

        createPanel()

        posI = 0
        posJ = 1
        posK = 2

        wait = false

        vector = Array(0) { Node(0,0,0,0, Color(0,0,0)) }

        thread = Thread(this)
    }

    /**
     * Setup the JPanel properties
     */
    private fun createPanel(){
        layout = null
        setSize(Dimension(600,300))
        background = secondary
    }


    fun createVector(){
        println("Creando vector")
        var size = 10
        println("Tamaño: ${size}")

        var tempPosX = 150
        var value = 0
        var nodeSize = 30
        vector = Array(size) { Node((1..10).random(), tempPosX, 100, nodeSize, principal ) }

        for (node in vector) {
            node.posX = tempPosX
            tempPosX += (nodeSize + 20)
        }
    }

    fun start(){
        this.thread.start()
    }

    fun bubble(){
        bubble(this.vector)
    }

    private fun bubble(vector: Array<Node>) {
        if(vector.size > 2){
            for(i in 1..vector.size - 1) {
                for(j in 0..vector.size - 1 - i){
                    println("I: ${posI} - ${i} === J: ${posJ} - ${j} === K: ${posK} - ${j+1}")
                    posI = i
                    posJ = j
                    posK = j + 1

                    if(vector[j].data > vector[j+1].data) {
                        change(j, j + 1)

                        Thread.sleep(1000)
                    }
                }
            }
        }


        this.thread = Thread(this)
    }

    private fun change(initialPos: Int, finalPos: Int){
        vector[initialPos].primary = this.arrows
        vector[finalPos].primary = this.arrows

        var aux = vector[initialPos].data
        vector[initialPos].data = vector[finalPos].data
        vector[finalPos].data = aux

        Thread.sleep(1000)

        vector[initialPos].primary = this.principal
        vector[finalPos].primary = this.principal
    }

    /**
    * Draw vector method
    * params: Graphics2D g , int[] vector
    * */
    fun drawVector(g: Graphics2D, vector: Array<Node>){
        for(node in vector){
            node.drawNode(g)
        }

        repaint()
    }

    private fun drawArrows(g: Graphics2D){
        var aux1 = vector[posI]
        var aux2 = vector[posJ]
        var aux3 = vector[posK]

        // Arrow one
        g.fillPolygon(intArrayOf(aux1.posX, aux1.posX + (aux1.tam / 2), aux1.posX + aux1.tam),
                intArrayOf(aux1.posY-10, aux1.posY, aux1.posY-10),3)
        g.fillRect(aux1.posX + 5, aux1.posY - 20, 20, 10)
        g.drawOval(aux1.posX, aux1.posY - 40, 30, 20)
        g.font = Font("Arial", Font.BOLD, 14)
        g.drawString("i", aux1.posX + 13, aux1.posY - 25)

        // Arrow two
        g.fillPolygon(intArrayOf(aux2.posX, aux2.posX + (aux2.tam / 2), aux2.posX + aux2.tam), intArrayOf(aux2.posY + aux2.tam + 10, aux2.posY + aux2.tam, aux2.posY + aux2.tam + 10), 3)
        g.fillRect(aux2.posX + 5, aux2.posY + aux2.tam + 10, 20, 10)
        g.drawOval(aux2.posX, aux2.posY + aux2.tam + 18, 30, 20)
        g.drawString("j", aux2.posX + 13, aux2.posY + aux2.tam + 32)

        // Arrow three
        g.fillPolygon(intArrayOf(aux3.posX, aux3.posX + (aux3.tam / 2), aux3.posX + aux3.tam), intArrayOf(aux3.posY + aux3.tam + 10, aux3.posY + aux3.tam, aux3.posY + aux3.tam + 10), 3)
        g.fillRect(aux3.posX + 5, aux3.posY + aux3.tam + 10, 20, 10)
        g.drawOval(aux3.posX, aux2.posY + aux3.tam + 18, 30, 20)
        g.drawString("j+1", aux3.posX + 8, aux3.posY + aux2.tam + 32)
    }

    override fun paintComponent(g: Graphics?) {
        super.paintComponent(g)

        var g2d = g as Graphics2D

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON)

        g2d.stroke = BasicStroke(4.0F)

        if (vector.size >= 2) {
            drawArrows(g2d);
            drawVector(g2d, vector);
        }

        repaint();
    }

    override fun run() {
        bubble()
    }
}
```

Archivo `Frame.kt`

```kotlin
package com.brayvasq.bubblesort.views

import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.event.ActionListener
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JPanel

/**
 * Frame that contains the elements of the App
 */
class Frame: JFrame() {
    var panel: Panel
    var acctions: JPanel
    var btnCreate: JButton
    var btnSort: JButton

    /**
     * Frame contructor
     * Setup the JFrame properties
     */
    init {
        panel = Panel()
        // Set windows title
        title = "BubbleSort"
        // Set window size
        size = Dimension(800,300)
        // Allow to close the window
        defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        // To center the window
        setLocationRelativeTo(null)
        // To not allow resize the window
        isResizable = false

        // Actions panel
        this.acctions = JPanel()

        this.btnCreate = JButton("Create")
        this.btnCreate.addActionListener(ActionListener {
            println("[Action] Click create")
            this.panel.createVector()
        })

        this.btnSort = JButton("Sort")
        this.btnSort.addActionListener {
            println("[Action] Click sort")
            this.panel.start()
        }

        this.acctions.add(this.btnCreate, BorderLayout.EAST)
        this.acctions.add(this.btnSort, BorderLayout.CENTER)

        add(this.acctions, BorderLayout.NORTH)

        // Setup panel
        add(panel, BorderLayout.CENTER)
        // To show the window
        isVisible = true
    }
}
```

Final structure

```bash
src/
├── main
│   ├── kotlin
│   │   └── com
│   │       └── brayvasq
│   │           └── bubblesort
│   │               ├── App.kt
│   │               ├── models
│   │               │   └── Node.kt
│   │               └── views
│   │                   ├── Frame.kt
│   │                   └── Panel.kt
│   └── resources
└── test
```

