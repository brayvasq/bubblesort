package com.brayvasq.bubblesort.models

import java.awt.Color
import java.awt.Graphics2D

/**
 * Class that represent a item from an array
 */
class Node(data: Int, posX: Int, posY: Int, tam: Int, primary: Color) {
    var data: Int = 0
    var posX: Int = 0
    var posY: Int = 0
    var tam:  Int = 0
    var primary: Color = Color(0,0,0)

    /**
     * Node class constructor
     */
    init {
        this.data = data
        this.posX = posX
        this.posY = posY
        this.tam  = tam
        this.primary = primary
    }

    /**
     * Function to draw a node
     */
    fun drawNode(g: Graphics2D){
        g.color = this.primary
        g.drawRect(this.posX - 2, this.posY, this.tam + 4, 24)

        g.color = this.primary
        g.drawString("${this.data}", this.posX + 10, this.posY + 15)
    }
}