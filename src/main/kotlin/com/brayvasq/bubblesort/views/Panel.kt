package com.brayvasq.bubblesort.views

import com.brayvasq.bubblesort.models.Node
import java.awt.*
import java.awt.RenderingHints
import javax.swing.JPanel


/**
 * Panel that contains the elements drawed
 */
class Panel: JPanel(), Runnable{

    /**
     * Styles and colors vars
     */
    var principal: Color
    var secondary: Color
    var square: Color
    var arrows: Color

    var wait: Boolean
    var thread: Thread
    var posI: Int
    var posJ: Int
    var posK: Int

    var vector: Array<Node>

    /**
     * Panel contructor
     */
    init {
        principal = Color(45,45,45)
        secondary = Color(236,240,241)
        square = Color(52, 152, 219)
        arrows = Color(231, 76, 60)

        createPanel()

        posI = 0
        posJ = 1
        posK = 2

        wait = false

        vector = Array(0) { Node(0,0,0,0, Color(0,0,0)) }

        thread = Thread(this)
    }

    /**
     * Setup the JPanel properties
     */
    private fun createPanel(){
        layout = null
        setSize(Dimension(600,300))
        background = secondary
    }


    /**
     * Ininitalize the vector var with some random numbers
     */
    fun createVector(){
        println("Creando vector")
        var size = 10
        println("Tamaño: ${size}")

        var tempPosX = 150
        var value = 0
        var nodeSize = 30
        vector = Array(size) { Node((1..10).random(), tempPosX, 100, nodeSize, principal ) }

        for (node in vector) {
            node.posX = tempPosX
            tempPosX += (nodeSize + 20)
        }
    }

    /**
     * Start the thread to excecute the bubblesort
     */
    fun start(){
        this.thread.start()
    }

    /**
     * Public function to execute the bubble function
     */
    fun bubble(){
        bubble(this.vector)
    }

    /**
     * Sort the vector elements usign the bubble sort algorithm
     */
    private fun bubble(vector: Array<Node>) {
        if(vector.size > 2){
            for(i in 1..vector.size - 1) {
                for(j in 0..vector.size - 1 - i){
                    println("I: ${posI} - ${i} === J: ${posJ} - ${j} === K: ${posK} - ${j+1}")
                    posI = i
                    posJ = j
                    posK = j + 1

                    if(vector[j].data > vector[j+1].data) {
                        change(j, j + 1)

                        Thread.sleep(1000)
                    }
                }
            }
        }


        this.thread = Thread(this)
    }

    /**
     * Switch the positions of two elements
     */
    private fun change(initialPos: Int, finalPos: Int){
        vector[initialPos].primary = this.arrows
        vector[finalPos].primary = this.arrows

        var aux = vector[initialPos].data
        vector[initialPos].data = vector[finalPos].data
        vector[finalPos].data = aux

        Thread.sleep(1000)

        vector[initialPos].primary = this.principal
        vector[finalPos].primary = this.principal
    }

    /**
    * Draw vector method
    * params: Graphics2D g , int[] vector
    * */
    fun drawVector(g: Graphics2D, vector: Array<Node>){
        for(node in vector){
            node.drawNode(g)
        }

        repaint()
    }

    /**
     * Draw arrows to show the index's positions
     */
    private fun drawArrows(g: Graphics2D){
        var aux1 = vector[posI]
        var aux2 = vector[posJ]
        var aux3 = vector[posK]

        // Arrow one
        g.fillPolygon(intArrayOf(aux1.posX, aux1.posX + (aux1.tam / 2), aux1.posX + aux1.tam),
                intArrayOf(aux1.posY-10, aux1.posY, aux1.posY-10),3)
        g.fillRect(aux1.posX + 5, aux1.posY - 20, 20, 10)
        g.drawOval(aux1.posX, aux1.posY - 40, 30, 20)
        g.font = Font("Arial", Font.BOLD, 14)
        g.drawString("i", aux1.posX + 13, aux1.posY - 25)

        // Arrow two
        g.fillPolygon(intArrayOf(aux2.posX, aux2.posX + (aux2.tam / 2), aux2.posX + aux2.tam), intArrayOf(aux2.posY + aux2.tam + 10, aux2.posY + aux2.tam, aux2.posY + aux2.tam + 10), 3)
        g.fillRect(aux2.posX + 5, aux2.posY + aux2.tam + 10, 20, 10)
        g.drawOval(aux2.posX, aux2.posY + aux2.tam + 18, 30, 20)
        g.drawString("j", aux2.posX + 13, aux2.posY + aux2.tam + 32)

        // Arrow three
        g.fillPolygon(intArrayOf(aux3.posX, aux3.posX + (aux3.tam / 2), aux3.posX + aux3.tam), intArrayOf(aux3.posY + aux3.tam + 10, aux3.posY + aux3.tam, aux3.posY + aux3.tam + 10), 3)
        g.fillRect(aux3.posX + 5, aux3.posY + aux3.tam + 10, 20, 10)
        g.drawOval(aux3.posX, aux2.posY + aux3.tam + 18, 30, 20)
        g.drawString("j+1", aux3.posX + 8, aux3.posY + aux2.tam + 32)
    }

    /**
     * Paint the arrows and vector
     */
    override fun paintComponent(g: Graphics?) {
        super.paintComponent(g)

        var g2d = g as Graphics2D

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON)

        g2d.stroke = BasicStroke(4.0F)

        if (vector.size >= 2) {
            drawArrows(g2d);
            drawVector(g2d, vector);
        }

        repaint();
    }

    /**
     * Run method overrides from Runnable
     */
    override fun run() {
        bubble()
    }
}