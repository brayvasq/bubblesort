package com.brayvasq.bubblesort.views

import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.event.ActionListener
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JPanel

/**
 * Frame that contains the elements of the App
 */
class Frame: JFrame() {
    var panel: Panel
    var acctions: JPanel
    var btnCreate: JButton
    var btnSort: JButton

    /**
     * Frame contructor
     * Setup the JFrame properties
     */
    init {
        panel = Panel()
        // Set windows title
        title = "BubbleSort"
        // Set window size
        size = Dimension(800,300)
        // Allow to close the window
        defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        // To center the window
        setLocationRelativeTo(null)
        // To not allow resize the window
        isResizable = false

        // Actions panel
        this.acctions = JPanel()

        this.btnCreate = JButton("Create")
        this.btnCreate.addActionListener(ActionListener {
            println("[Action] Click create")
            this.panel.createVector()
        })

        this.btnSort = JButton("Sort")
        this.btnSort.addActionListener {
            println("[Action] Click sort")
            this.panel.start()
        }

        this.acctions.add(this.btnCreate, BorderLayout.EAST)
        this.acctions.add(this.btnSort, BorderLayout.CENTER)

        add(this.acctions, BorderLayout.NORTH)

        // Setup panel
        add(panel, BorderLayout.CENTER)
        // To show the window
        isVisible = true
    }
}